function operate(num1, num2, operator) {
    if (operator == "add") {
        return num1 + num2;
    } else if (operator == "subtract") {
        return num1 - num2;
    } else if (operator == "multiply") {
        return num1 * num2;    
    } else if (operator == "divide") {
        return num1 / num2;
    } 
}

function calculate() {
        var num1 = parseInt(validate(document.getElementById("num1").value));
        var num2 = parseInt(validate(document.getElementById("num2").value));
        var operator = document.getElementById('operator').value;
        document.getElementById("result").value = operate(num1, num2, operator);
}

function validate(value) {
    if (value == "") {
        alert("2 numbers are required");
        return 0;
        } else return value;
}